package pos.machine;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class PosMachine {
    public String printReceipt(List<String> barcodes) {
        return getReceipt(barcodes);
    }

    boolean isTheBarcodesNull(List<String> barcodes){
        return barcodes.isEmpty();
    }

    boolean isTheBarcodeValid(String barcode,List<Item> items){
        return items.stream().anyMatch(item -> item.getBarcode().equals(barcode));
    }

    Item readOneBarcode(String barcode, List<Item> items){
        return items.stream().filter(item -> item.getBarcode().equals(barcode)).findFirst().get();
    }

    Map<Item,Integer> recordQuantity(Item item, Map<Item, Integer> quantityOfItems){
        int num;
        if(quantityOfItems.containsKey(item)){
            num = quantityOfItems.get(item);
            quantityOfItems.replace(item,num+1);
        }else{
            quantityOfItems.put(item,1);
        }
        return quantityOfItems;
    }

    Map<Item,Integer>  scanBarcodes(List<String> barcodes,List<Item> items){
        if(isTheBarcodesNull(barcodes))
            return null;

        Map<Item,Integer> quantityOfItems = new HashMap<>();


        for (String barcode : barcodes) {
            if(!isTheBarcodeValid(barcode,items))
                continue;

            Item item = readOneBarcode(barcode,items);
            recordQuantity(item,quantityOfItems);

        }
        return quantityOfItems;
    }

    boolean isTheQuantityMapNull(Map<Item,Integer> quantityOfItems){
        return quantityOfItems.isEmpty();
    }

    String generateReceiptHead(){
        return "***<store earning no money>Receipt***\n";
    }

    String generateReceiptFoot(Map<Item,Integer> quantityOfItems){
        int total = 0;
        for(Map.Entry<Item,Integer> entry:quantityOfItems.entrySet()){
            total+=entry.getKey().getPrice()*entry.getValue();
        }
        return "----------------------\n" +
                "Total: "+total+" (yuan)\n**********************";
    }

    String generateReceiptBody(Map<Item,Integer> quantityOfItems,List<Item> items){
        String body = "";
        for(Item item:items){
            body+="Name: "+item.getName()+", Quantity: "
                    +quantityOfItems.get(item)+", Unit price: "
                    +item.getPrice()+" (yuan), Subtotal: "
                    +quantityOfItems.get(item)*item.getPrice()+" (yuan)\n";
        }
        return body;
    }

    String generateReceipt(Map<Item,Integer> quantityOfItems,List<Item> items){
        if(isTheQuantityMapNull(quantityOfItems))
            return null;

        String receipt = generateReceiptHead()
                +generateReceiptBody(quantityOfItems,items)
                +generateReceiptFoot(quantityOfItems);

        return receipt;

    }

    String getReceipt(List<String> barcodes){
        List<Item> items = ItemsLoader.loadAllItems();
        Map<Item,Integer> quantityOfItems = scanBarcodes(barcodes,items);
        return generateReceipt(quantityOfItems,items);
    }


}
