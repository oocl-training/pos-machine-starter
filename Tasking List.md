1. read barcodes until all barcodes are read out and record the quantity of these items.
    input: barcodes:List<String>, items:List<Item>
    output: quantityOfItems:Map<Item, Integer>

    1.1. check if the barcodes is null.
        input: barcodes:List<String>
        output: isNull:boolean

    1.2. check if the barcode is valid.
        input: barcodes:String, items:List<Item>
        output: isValid:boolean

    1.3. read a barcodes, map barcode to item.
        input: barcode:String, items:List<Item>
        output: item:String

    1.4. record the quantity of this item.
        input: item:String, quantityOfItems:Map<Item, Integer>
        output: quantityOfItems:Map<Item, Integer>

2. generate a receipt
    input: quantityOfItems:Map<String, Integer>
    output: receipt:String

    2.1 check if the Map is null
        input: quantityOfItems:Map<String, Integer>
        output: isNull:boolean
    
    2.2 generate the head of the receipt
        input: void
        output: head:String

    2.3 generate the main body of the receipt
        input: quantityOfItems:Map<String, Integer>, items:List<Item>
        output: body:String

    2.4 generate the foot of the receipt.
        input: quantityOfItems:Map<Item, Integer>
        output: foot:String