## O (Objective)
What did we learn today? What activities did you do? What scenes have impressed you?
```
1. learn Tasking list
2. learn Context Map
3. practise tasking list and context map about elephant, cute number, mulitiplication and pos-machine.
4. the pos-machine impress me.
```
## R (Reflective)
Please use one word to express your feelings about today's class.
```
tired
```
## I (Interpretive)
What do you think about this? What was the most meaningful aspect of this activity?
```
1. meaningful for convert major problems into multiple sub problems while developing.
2. how to sovle a major problems and how to clarify the requirement further.
```
## D (Decisional)
Where do you most want to apply what you have learned today? What changes will you make?
```
1. when solving a major problem that can not be solved in one step
2. Think more and provide additional information
```